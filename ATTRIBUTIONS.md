
 * **Public Domain**: AOP alliance
 * **Unknown**: SLF4J API Module, SLF4J LOG4J-12 Binding, jstl
 * **The Apache Software License, Version 2.0**: Apache Commons Logging, Apache Log4j, Spring AOP, Spring Beans, Spring Context, Spring Core, Spring Expression Language (SpEL), Spring JDBC, Spring Transaction, Spring Web, Spring Web MVC, spring-security-acl, spring-security-config, spring-security-core, spring-security-taglibs, spring-security-web