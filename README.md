# Demonstration web application of a small set of Spring Security features.

## Outline

With a few file modifications the source code when compiled should generate a `.war` file with the
intention of providing a very simple demonstration of the [spring security filters](http://docs.spring.io/spring-security/site/docs/3.0.x/reference/security-filter-chain.html)
in operation.

## Installation

 * Copy `src/properties/sample.filter.properties` to `src/properties/filter.properties` and edit
   the content according to your environment.
 * Copy `src/main/webapp/WEB-INF/spring/sample.root-context.site.xml` to
   `src/main/webapp/WEB-INF/spring/root-context.site.xml` and edit. (Note: The default code should
   work without modification).
 * Run Maven's `mvn clean verify` in the directory where `pom.xml` exists. This should generate a
   file called `sse.war` in the `/target` directory.
 * Copy the `sse.war` file to whichever servlet container (e.g. Tomcat) you have.

## Running

 * Start the servlet container.
 * Visit the default location in a browser, e.g. `http://localhost:8080/sse/`.
 * View the resultant log files to see what's happened.