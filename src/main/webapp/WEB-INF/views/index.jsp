<%@page session="false" %>
<%
  response.setHeader("Expires", "Sat, 6 May 1995 12:00:00");
  response.setHeader("Cache-control", "private, no-cache, no-store, must-revalidate");
  response.setHeader("Cache-control", "post-check=0, precheck=0");
  response.setHeader("Pragma", "no-cache");
  response.setDateHeader("Expires", 0);

  response.setCharacterEncoding("utf-8");
  response.setContentType("text/html;charset=UTF-8");
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="uk.ac.ox.cs.compbio.sse.SSEIdentifiers" %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
  <head>
    <link href="<c:url value="/resources/css/site.css" />" media="all" rel="stylesheet" type="text/css" />
    <title>Demonstration Spring Security Application</title>
  </head>
  <body>

    <div id="header">
      <img alt="Oxford University Computer Science Dept. logo."
           src="<c:url value="/resources/img/CompSci_logo_landscapeL_rgb_corrected.jpg" />" />
      <a href="<%= SSEIdentifiers.VIEW_ABOUT %>"
         title="Visit the About page"> About </a>
    </div>

    <div class="main">
      Hello!
    </div>
  </body>
</html>
