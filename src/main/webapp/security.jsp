<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
  //Set to expire far in the past
  response.setHeader("Expires", "Sat, 6 May 1995 12:00:00");
  //Standard HTTP 1.1
  response.setHeader("Cache-Control", "private, no-cache, no-store, must-revalidate");
  //Set IE extended HTTP 1.1 no-cache headers
  response.addHeader("Cache-Control", "post-check=0, precheck=0");
  //Standard HTTP 1.0
  response.setHeader("Pragma", "no-cache");
  //prevents caching at the proxy server
  response.setDateHeader ("Expires", 0);
%>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <link type="text/css" href="<c:url value="/resources/css/client.css" />" rel="stylesheet" />

    <title>Security Issue</title>
  </head>
  <body>
    <div>
      <h1>A security exception has occurred in the sample application!</h1>
      <input type="button" value=" Back " onClick="history.back()" />
    </div>
  </body>
</html>